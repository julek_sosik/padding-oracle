# coding: utf-8

from Crypto.Cipher import AES
import os,socket,threading
import binascii
import StringIO
import base64

key = 'This is a key123'
iv = 'This is an IV456'
local_ip = socket.gethostbyname(socket.gethostname())
local_port = 8888

class PKCS7Encoder(object):
    def __init__(self, k=16):
       self.k = k

    ## @param text The padded text for which the padding is to be removed.
    # @exception ValueError Raised when the input padding is missing or corrupt.
    def decode(self, text):
        '''
        Remove the PKCS#7 padding from a text string
        '''
        nl = len(text)
        val = int(binascii.hexlify(text[-1]), 16)
        if val == 0:
            raise ValueError('Input is not padded or padding is corrupt')
        if val > self.k:
            raise ValueError('Input is not padded or padding is corrupt')
        for i in range(2, val+1):
            if int(binascii.hexlify(text[-i]), 16) != val:
                raise ValueError('Input is not padded or padding is corrupt')
            
        l = nl - val
        print(text)
        return text[:l]

    ## @param text The text to encode.
    def encode(self, text):
        '''
        Pad an input string according to PKCS#7
        '''
        l = len(text)
        output = StringIO.StringIO()
        val = self.k - (l % self.k)
        for _ in xrange(val):
            output.write('%02x' % val)
        return text + binascii.unhexlify(output.getvalue())

class serverThread(threading.Thread):
    def __init__(self,sock_info):
        self.conn=sock_info[0]
        self.addr=sock_info[1]
        self.key = key
        self.iv = iv
        threading.Thread.__init__(self)
        
    def run(self):
        while 1:
            cmd = self.conn.recv(4096)
            if cmd == 'e':
                self.conn.send('Encrypt what?')
                data = self.conn.recv(4096)
                aesCryptoObj = AES.new(self.key, AES.MODE_CBC, self.iv)
                print(data)
                try:
                    data_padded = PKCS7Encoder().encode(data)
                    cipherTxt = aesCryptoObj.encrypt(data_padded)
                    cipherTxt = base64.b64encode(cipherTxt)
                except:
                    cipherTxt = 'ERROR'
                    print('ERROR')
                self.conn.send(cipherTxt)
            elif cmd == 'd':
                self.conn.send('Decrypt what?')
                cipherTxt = self.conn.recv(4096)
                aesCryptoObj = AES.new(self.key, AES.MODE_CBC, self.iv)
                #print(cipherTxt)
                try:
                    data_padded = aesCryptoObj.decrypt(base64.b64decode(cipherTxt))
                    data = PKCS7Encoder().decode((data_padded))
                    print(binascii.hexlify(bytearray(data)))
                    data = 'ok'
                except:
                    data = 'ERROR'
                    #print('ERROR')
                self.conn.send(data)
            elif cmd == 'iv':
                self.conn.send('Set IV to what?')
                iv_temp = self.conn.recv(4096)
                if len(iv_temp) != 16:
                    self.conn.send('ERROR')
                else:
                    self.iv = iv_temp
                    self.conn.send('ok')
            else:
                self.conn.send('wrong cmd')
            

class server(threading.Thread):
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((local_ip,local_port))
        threading.Thread.__init__(self)

    def run(self):
        self.sock.listen(5)
        while 1:
            proc=serverThread(self.sock.accept())
            proc.daemon=True
            proc.start()

    def stop(self):
        self.sock.close()


cryptoServer=server()
cryptoServer.daemon=True
cryptoServer.start()
print('On', local_ip, ':', local_port)
raw_input('Enter to end...\n')
cryptoServer.stop()